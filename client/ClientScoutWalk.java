/*******************************************************************************
 * Brendan Donohoe and Robert Nicholson
 * Original Author: Joel Castellanos
 * 
 * ClientScoutWalk contains the main method for the antworld project.
 * It contains some AI methods and server connection/updating methods.
 * To run, make sure that AntWorld.png is saved in this file's parent folder.
 * If the connection is failing, try changing the serverhost value in main.
 ******************************************************************************/

package antworld.client;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Random;

import javax.imageio.ImageIO;

import antworld.control.Util;
import antworld.control.ScoutData;
import antworld.data.AntAction;
import antworld.data.AntAction.AntActionType;
import antworld.data.AntData;
import antworld.data.AntType;
import antworld.data.CommData;
import antworld.data.Constants;
import antworld.data.NestNameEnum;
import antworld.data.TeamNameEnum;
import antworld.gui.*;

public class ClientScoutWalk
{
  /**The maximum number of ants we will make before investing points in score
   * rather than ant-making.
   */
  public static final int MAX_ANTS = 250;
  
  private static final boolean DEBUG = false;
  private static final TeamNameEnum myTeam = TeamNameEnum.Windmillgrass;
  private static final long password = 495149740275L; //Each team has been assigned a random password.
  private BufferedImage map = null;
  private ObjectInputStream inputStream = null;
  private ObjectOutputStream outputStream = null;
  private boolean isConnected = false;
  private NestNameEnum myNestName = null;
  private int centerX, centerY;
  private AntFrame frame;
  private ArrayList<ScoutData> scoutList = null;

  private Socket clientSocket;

  private static Random random = Constants.random;

//******************************************************************************
// String host, int portNumber
// host should be the name of the server
// portNumber is the port for the client to connect to
// Constructor method for ClientRandomWalk. 
// It attempts to open the map for Antworld and calls for server connection.
//******************************************************************************
  public ClientScoutWalk(String host, int portNumber) throws InvalidIDException
  {
    Util.initTable();
    try
    {
      map = ImageIO.read(new File("AntWorld.png"));
    }
    catch (IOException e)
    {
      System.err.println("ClientScoutWalk: ERROR: Map not found");
      System.exit(1);
    }
    System.out.println("Starting ClientScoutWalk: " + System.currentTimeMillis());
    isConnected = false;
    while (!isConnected)
    {
      isConnected = openConnection(host, portNumber);
      if (!isConnected) try { Thread.sleep(1000); } catch (InterruptedException e1) {}
    }
    CommData data = chooseNest();
    frame = new AntFrame(data); //Bring up the GUI JFrame.
    mainGameLoop(data);
    closeAll();
  }
//******************************************************************************
//String host, int portNumber
//host should be the name of the server
//portNumber is the port for the client to connect to
//returns a boolean indicating if the connection was successful
//Method for testing connection to another server
//Attempts to connect, and if successful returns true. Otherwise, error.
//******************************************************************************
  private boolean openConnection(String host, int portNumber)
  {
    System.out.println("Entering openConnection...");
    try
    {
      clientSocket = new Socket(host, portNumber);
      System.out.println("Opening socket...");
    }
    catch (UnknownHostException e)
    {
      System.err.println("ClientScoutWalk Error: Unknown Host " + host);
      e.printStackTrace();
      return false;
    }
    catch (IOException e)
    {
      System.err.println("ClientScoutWalk Error: Could not open connection to " + host + " on port " + portNumber);
      e.printStackTrace();
      return false;
    }

    try
    {
      System.out.println("Opening I/O streams...");
      outputStream = new ObjectOutputStream(clientSocket.getOutputStream());
      System.out.println("Output stream opened!");
      inputStream = new ObjectInputStream(clientSocket.getInputStream());
      System.out.println("Input stream opened!");
    }
    catch (IOException e)
    {
      System.err.println("ClientScoutWalk Error: Could not open i/o streams");
      e.printStackTrace();
      return false;
    }

    return true;

  }

//******************************************************************************
//No input, no return value
//Closes the connection to the client
//******************************************************************************
  public void closeAll()
  {
    System.out.println("ClientScoutWalk.closeAll()");
    {
      try
      {
        if (outputStream != null) outputStream.close();
        if (inputStream != null) inputStream.close();
        clientSocket.close();
      }
      catch (IOException e)
      {
        System.err.println("ClientScoutWalk Error: Could not close");
        e.printStackTrace();
      }
    }
  }

//******************************************************************************
//No input
//Return CommData, which is combination of team data and nest selected.
//Sets password and does error checking for incorrect data transfer
//******************************************************************************
  public CommData chooseNest()
  {
    while (myNestName == null)
    {
      try { Thread.sleep(100); } catch (InterruptedException e1) {}
        
      //NestNameEnum requestedNest = NestNameEnum.values()[random.nextInt(NestNameEnum.SIZE)];
      NestNameEnum requestedNest = NestNameEnum.LEPTOTHORAX;
      CommData data = new CommData(requestedNest, myTeam);
      data.password = password;
      
      if( sendCommData(data) )
      {
        try
        {
          if (DEBUG) System.out.println("ClientScoutWalk: listening to socket....");
          CommData recvData = (CommData) inputStream.readObject();
          if (DEBUG) System.out.println("ClientScoutWalk: recived <<<<<<<<<"+inputStream.available()+"<...\n" + recvData);
          
          if (recvData.errorMsg != null)
          {
            System.err.println("ClientScoutWalk***ERROR***: " + recvData.errorMsg);
            continue;
          }
  
          if ((myNestName == null) && (recvData.myTeam == myTeam))
          { myNestName = recvData.myNest;
            centerX = recvData.nestData[myNestName.ordinal()].centerX;
            centerY = recvData.nestData[myNestName.ordinal()].centerY;
            System.out.println("ClientScoutWalk: !!!!!Nest Request Accepted!!!! " + myNestName);
            return recvData;
          }
        }
        catch (IOException e)
        {
          System.err.println("ClientScoutWalk***ERROR***: client read failed");
          e.printStackTrace();
        }
        catch (ClassNotFoundException e)
        {
          System.err.println("ClientScoutWalk***ERROR***: client sent incorect data format");
        }
      }
    }
    return null;
  }

//******************************************************************************
//Inputs: CommData data, the communication data
//No return value.
//Initializes scout data and calls for AI. Calls to sendCommData.
//******************************************************************************
  public void mainGameLoop(CommData data) throws InvalidIDException
  {
    while (true)
    {
      try
      {
        if (DEBUG) System.out.println("ClientScoutWalk: chooseActions: " + myNestName);

        if (data.myAntList != null && data.myAntList.size() > 0 && scoutList == null)
        {
          initializeScoutList(data);
        }
        chooseActionsOfAllAnts(data);	//call for AI

        CommData sendData = data.packageForSendToServer();
        
        //System.out.println("ClientScoutWalk: Sending>>>>>>>: " + sendData);
        outputStream.writeObject(sendData);
        outputStream.flush();
        outputStream.reset();
       

        if (DEBUG) System.out.println("ClientScoutWalk: listening to socket....");
        CommData recivedData = (CommData) inputStream.readObject();
        if (DEBUG) System.out.println("ClientScoutWalk: received <<<<<<<<<"+inputStream.available()+"<...\n" + recivedData);
        data = recivedData;
        updateScoutList(data);
        frame.update(data);
  
        
        if ((myNestName == null) || (data.myTeam != myTeam))
        {
          System.err.println("ClientScoutWalk: !!!!ERROR!!!! " + myNestName);
        }
      }
      catch (IOException e)
      {
        System.err.println("ClientScoutWalk***ERROR***: client read failed");
        e.printStackTrace();
        try { Thread.sleep(1000); } catch (InterruptedException e1) {}

      }
      catch (ClassNotFoundException e)
      {
        System.err.println("ServerToClientConnection***ERROR***: client sent incorect data format");
        e.printStackTrace();
        try { Thread.sleep(1000); } catch (InterruptedException e1) {}
      }

    }
  }
  
//******************************************************************************
//Input: CommData data, the communications data.
//Returns: true or false, depending on if there was an error or not.
//Attempts to send out data to the server
//******************************************************************************
  private boolean sendCommData(CommData data)
  {
    
    CommData sendData = data.packageForSendToServer();
    try
    {
      if (DEBUG) System.out.println("ClientScoutWalk.sendCommData(" + sendData +")");
      outputStream.writeObject(sendData);
      outputStream.flush();
      outputStream.reset();
    }
    catch (IOException e)
    {
      System.err.println("ClientScoutWalk***ERROR***: client read failed");
      e.printStackTrace();
      try { Thread.sleep(1000); } catch (InterruptedException e1) {}
      return false;
    }

    return true;
    
  }

//******************************************************************************
//Input: CommData commData
//Returns: none
//Calls for AI for each ant
//******************************************************************************
  private void chooseActionsOfAllAnts(CommData commData)
  {
    //First, call for the action of each ant.
    for (int i = 0; i < commData.myAntList.size(); i++)
    {
      AntData ant = commData.myAntList.get(i);
      AntAction action = chooseAction(commData, ant, i);
      ant.myAction = action;
    }
    
    //Birth ants and add to main AntData list before shipping commData back to
    //the server if we have less than MAX_ANTS ants.  In the event that we have
    //a MAX_ANTS number of ants, the client will still attempt to birth one ant
    //of each type once that number dips below.
    if (commData.myAntList.size() < MAX_ANTS)
    {
      for (AntType a : AntType.values())
      {
        for (int totalFood = commData.foodStockPile[a.ordinal() + 2]; totalFood >= 100; totalFood -= 100)
        {
          commData.myAntList.add(new AntData(Constants.UNKNOWN_ANT_ID, 
                                             AntType.values()[a.ordinal()],
                                             commData.myNest, commData.myTeam));

          if (commData.myAntList.size() >= MAX_ANTS)
          {
            //Doesn't break out of the outer loop, so we'll continue through
            //for each antType, ideally birthing one of each to keep things
            //balanced.
            
            break;
          }
        }
      }
    }
  }
  
//******************************************************************************
//Input: CommData data, AntData ant, int index
//Returns: AntAction
//Sets the action for an ant
//Completely random, biased towards moving close to base.
//******************************************************************************
  private AntAction chooseAction(CommData data, AntData ant, int index)
  {
    if (DEBUG) System.out.println("ClientScoutWalk: Prev Move: " + ant.myAction);
    
    //If the ant has died, do nothing.
    if (ant.myAction.type.equals(AntActionType.DIED))
    {
      return ant.myAction;
    }
    
    AntAction action = new AntAction(AntActionType.STASIS);
    
    if (ant.ticksUntilNextAction > 0)
    {
      if (DEBUG) System.out.println("ClientScoutWalk: Ticks ran out: Next Move: " + action);
      return action;
    }
    
    //If the ant is underground, heal if not at full health.  Otherwise, come
    //out at a random spot in the nest.
    if (ant.underground)
    {
      if (ant.health >= ant.antType.getMaxHealth())
      {
        action.type = AntActionType.EXIT_NEST;
        action.x = centerX - Constants.NEST_RADIUS + random.nextInt(2 * Constants.NEST_RADIUS);
        action.y = centerY - Constants.NEST_RADIUS + random.nextInt(2 * Constants.NEST_RADIUS);
        return action;
      }
      else
      {
        action.type = AntActionType.HEAL;
        return action;
      }
    }
    
    long curTime;
    if (DEBUG) curTime = System.nanoTime();
    
    //Get the next move from this AntData's corresponding ScoutData.
    action = scoutList.get(index).nextMove();
    
    if (DEBUG) System.out.println("ClientScoutWalk: Total time for move: " + (System.nanoTime() - curTime) + " ns");
    if (DEBUG) System.out.println("ClientScoutWalk: Next move: " + action);
    return action;
  }
  
  /**
   * Initializes the corresponding scout data list so that it may be used in
   * conjunction with the main list to decide moves.
   * @param data The data from which the main AntList will be read.
   */
  private void initializeScoutList(CommData data)
  {
    scoutList = new ArrayList<ScoutData>();
    for (AntData a : data.myAntList)
    {
      scoutList.add(new ScoutData(a, map, data));
    }
  }
  
  /**
   * Updates the main scout list with new information returned from the server.
   * Run after synchronizeScoutList has taken out the dead ants from the
   * previous version of the list.
   * @param data The data from which the main AntList will be read.
   */
  private void updateScoutList(CommData data) throws InvalidIDException
  {
    //Add new ants if there are any (in this case, if myAntList has a greater
    //size than the scoutList updated previously).
    
    for (int i = scoutList.size(); i < data.myAntList.size(); i++)
    {
      System.out.println("ClientScoutWalk: Birthing " + data.myAntList.get(i));
      scoutList.add(new ScoutData(data.myAntList.get(i), map, data));
    }

    for (int i = 0; i < scoutList.size(); i++)
    {
      try
      {
        while (scoutList.get(i).id != data.myAntList.get(i).id)
        {
          if (DEBUG) System.out.println("ClientScoutWalk:" + data.myAntList.get(i).id + "===>" + scoutList.get(i).id);
          scoutList.remove(i);
        }
        if (DEBUG) System.out.println("ClientScoutWalk: " + data.myAntList.get(i).id + "===>" + scoutList.get(i).id);
        if (scoutList.get(i).id != data.myAntList.get(i).id)
        {
          throw new InvalidIDException();
        }
        scoutList.get(i).update(data.myAntList.get(i), data);
      }
      catch (IndexOutOfBoundsException e)
      {
        e.printStackTrace();
        scoutList.remove(i);
      }
    }
  }

  public static void main(String[] args)
  {
    String serverHost = "b146-69";
    if (args.length > 0) serverHost = args[0];
    while (true)
    {
      try
      {
        new ClientScoutWalk(serverHost, Constants.PORT);
      }
      catch (Exception e)
      {
        e.printStackTrace();
      }
    }
  }
}