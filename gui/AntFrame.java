package antworld.gui;
import antworld.data.*;
import javax.swing.*;

public class AntFrame extends JFrame
{
  AntPane pane = new AntPane(null);
  public AntFrame(CommData antsData)
  {
    setTitle("It's an Ant's World!");
    setDefaultCloseOperation(EXIT_ON_CLOSE);
    setLocationRelativeTo(null);
    pane.update(antsData);
    getContentPane().add(pane);
    pack();
    setVisible(true);
  }
  
  public void update(CommData antsData)
  {
    pane.update(antsData);
  }
}