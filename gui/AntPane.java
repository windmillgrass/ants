/*******************************************************************************
 * AntPane extends JPanel and controls what happens in the GUI. Contains a mouse
 * listener for moving the gigantic 5000x2500 picture around.
 ******************************************************************************/
 package antworld.gui;

import antworld.data.*;

import javax.imageio.*;
import javax.swing.*;
import javax.swing.Timer;

import java.awt.image.*;
import java.awt.*;
import java.awt.event.*;
import java.io.*;
import java.util.*;

public class AntPane extends JPanel implements ActionListener, MouseListener, 
                                                   MouseMotionListener
{
  private CommData gameData;
  private BufferedImage map;
  private Image savedMapImage;
  private int cameraX;
  private int cameraY;
  private int mouseOriginX = -1;
  private int mouseOriginY = -1;
  private int cameraOriginX;
  private int cameraOriginY;
  
  /* Constructor for AntPane
   * @param CommData gameData, the info retrieved from the server
   * @return none
   */
  public AntPane(CommData gameData)
  {
    this.gameData = gameData;
    
    try
    {
      File antworldFile = new File("AntWorld.png");
      this.savedMapImage = ImageIO.read(antworldFile);
      this.map = ImageIO.read(antworldFile);
    }
    catch (IOException e)
    {
      e.printStackTrace();
      System.exit(1);
    }
    setPreferredSize(new Dimension (5000, 2500));
    Timer antTimer = new Timer(10, this);
    antTimer.start();
    addMouseListener(this);
    addMouseMotionListener(this);
    repaint();
  }
  
  /* updates information for the game
   * @param CommData gameData, the info retrieved from the server (agin)
   * @return none
   */  
  public void update(CommData updatedData)
  {
    this.gameData = updatedData;
  }
  
  /* Paints the map picture and each ant, food, etc. on it.
   * Also displays how many food boxes, score and friendlies are visible.
   * @param Graphics g
   * @return none
   */
  protected void paintComponent(Graphics g)
  {
    Graphics g2 = map.getGraphics();
    g2.drawImage(savedMapImage, 0, 0, map.getWidth(), map.getHeight(), this);
    ArrayList<AntData> antList = gameData.myAntList;
    if (antList != null)
    {
      for (AntData a : antList)
      {
        if (!a.underground)
        {
          if (a.carryUnits > 0)
          {
            if (a.carryType.equals(FoodType.WATER))
            {
              g2.setColor(Color.WHITE);
            }
            else
            {
              g2.setColor(Color.DARK_GRAY);
            }
          }
          else
          {
            if (a.antType.equals(AntType.BASIC))
            {
              g2.setColor(Color.BLACK);
            }
            else if (a.antType.equals(AntType.ATTACK))
            {
              g2.setColor(Color.RED);
            }
            else if (a.antType.equals(AntType.DEFENCE))
            {
              g2.setColor(Color.CYAN);
            }
            else if (a.antType.equals(AntType.MEDIC))
            {
              g2.setColor(Color.BLUE);
            }
            else if (a.antType.equals(AntType.CARRY))
            {
              g2.setColor(Color.YELLOW);
            }
            else if (a.antType.equals(AntType.SPEED))
            {
              g2.setColor(Color.ORANGE);
            }
            else if (a.antType.equals(AntType.VISION))
            {
              g2.setColor(Color.MAGENTA);
            }
          }
          g2.fillRect(a.gridX - 2, a.gridY - 2, 5, 5);
        }
      }
    }
    
    g2.setColor(Color.RED);
    HashSet<AntData> enemy = gameData.enemyAntSet;
    if (enemy != null)
    {
      for (AntData a : enemy)
      {
        if (!a.underground)
        {
          g2.fillOval(a.gridX - 1, a.gridY - 1, 3, 3);
        }
      }
    }
    
    g2.setColor(Color.MAGENTA);
    HashSet<FoodData> food = gameData.foodSet;
    if (food != null)
    {
      for (FoodData f : food)
      {
        if (f.foodType.equals(FoodType.BASIC))
        {
          g2.setColor(Color.BLACK);
        }
        else if (f.foodType.equals(FoodType.ATTACK))
        {
          g2.setColor(Color.RED);
        }
        else if (f.foodType.equals(FoodType.DEFENCE))
        {
          g2.setColor(Color.CYAN);
        }
        else if (f.foodType.equals(FoodType.MEDIC))
        {
          g2.setColor(Color.BLUE);
        }
        else if (f.foodType.equals(FoodType.CARRY))
        {
          g2.setColor(Color.YELLOW);
        }
        else if (f.foodType.equals(FoodType.SPEED))
        {
          g2.setColor(Color.ORANGE);
        }
        else if (f.foodType.equals(FoodType.VISION))
        {
          g2.setColor(Color.MAGENTA);
        }
        else if (f.foodType.equals(FoodType.WATER))
        {
          g2.setColor(Color.WHITE);
        }
        g2.fillRoundRect(f.gridX - 2, f.gridY - 2, 5, 5, 2, 2);
      }
    }
    g.setColor(Color.WHITE);
    g.drawImage(map, cameraX, cameraY, this);
    g.setFont(new Font("AntFont", Font.BOLD, 32));
    if (antList != null)
    {
      g.drawString("Ants: " + antList.size(), 0, getHeight());
    }
    if (enemy != null)
    {
      g.drawString("Enemy!: " + enemy.size(), getWidth() / 2, getHeight());
    }
    if (food != null)
    {
      g.drawString("Food in Play: " + food.size(), 0, 32);
    }
    if (gameData.foodStockPile != null)
    {
      int score = 0;
      for (int n : gameData.foodStockPile)
      {
        score += n;
      }
      score -= gameData.foodStockPile[FoodType.WATER.ordinal()];
      g.drawString("Score: " + score, getWidth() / 2, 32);
    }
    if (gameData.foodStockPile[FoodType.WATER.ordinal()] != 0)
    {
      int totalWater = gameData.foodStockPile[FoodType.WATER.ordinal()];
      g.drawString("Water: " + totalWater, getWidth() - getWidth()/4, 32);
    }
  }
  
  public void actionPerformed(ActionEvent evt)
  {
    repaint();
  }
  
  public void mousePressed(MouseEvent evt)
  {
    if (mouseOriginX == -1 && mouseOriginY == -1)
    {
      mouseOriginX = evt.getX();
      mouseOriginY = evt.getY();
      cameraOriginX = cameraX;
      cameraOriginY = cameraY;
    }
  }
  
  /* Sets the mouse movement to stasis when the mouse is released
   * @param MouseEvent evt, the mouse movement reader
   * @return none
   */
  public void mouseReleased(MouseEvent evt)
  {
    mouseOriginX = -1;
    mouseOriginY = -1;
  }
  
  /* Move the map camera by draggin the mouse
   * @param MouseEvent evt, the mouse movement reader
   * @return none
   */
  public void mouseDragged(MouseEvent evt)
  {
    cameraX = cameraOriginX - (mouseOriginX - evt.getX());
    cameraY = cameraOriginY - (mouseOriginY - evt.getY());
    if (cameraX  > 0)
    {
      cameraX = 0;
    }
    if (cameraY > 0)
    {
      cameraY = 0;
    }
    if (cameraX < -(map.getWidth(null) - getWidth()))
    {
      cameraX = -(map.getWidth(null) - getWidth());
    }
    if (cameraY < -(map.getHeight(null) - getHeight()))
    {
      cameraY = -(map.getHeight(null) - getHeight());
    }
  }
  
  //Unused extra MouseListener methods.
  public void mouseMoved(MouseEvent evt) {}
  /*include way to cycle through ants?*/
  public void keyReleased(KeyEvent evt) {}
  public void mouseEntered(MouseEvent evt) {}
  public void mouseExited(MouseEvent evt) {}
  public void mouseClicked(MouseEvent evt) {}
}