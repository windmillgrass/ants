AntWorld was coded by Brendan Donohoe and Robert Nicholson
The main entry point is located in ClientScoutWalk.java.
No special VM options are needed for compiling.

To connect to a different server, change the serverHost string in ClientScoutWalk.java.
To change team name and password, change myTeam and password in ClientScoutWalk.java.
By default, the team is Windmillgrass connecting to  server b149-69.

Please keep AntWorld.png in the top folder of the working directory.
For Eclipse, this would be in the same folder as src.
If AntWorld.png is not in the correct directory, the program will inform you and close.
