/*******************************************************************************
 * Implementation of the A-star path finding method. Each ant will use this 
 * class to determine the shortest path to noteworthy points, like food boxes 
 * and the nest.
 ******************************************************************************/

package antworld.control;
import antworld.control.LocationNode;
import antworld.data.*;

import java.util.*;
import java.awt.*;
import java.awt.image.*;

public class Util
{
  public static final int WATER_RGB = -14774017;
  public static final int NEST_RGB = -989556;
  
  // Java takes a while to find square roots, so we find them once at the start.
  private static short [] sqrtTable = new short [ScoutData.SEARCH_RADIUS * ScoutData.SEARCH_RADIUS];
  private static boolean DEBUG = false;
  
  /**
   * Called at the very beginning, before the connection is made to the server,
   * to initialize a look up table for integer-precision square root values to
   * cut down on computation time.
   */
  public static void initTable()
  {
    for (int i = 0; i < sqrtTable.length; i++)
    {
      sqrtTable[i] = (short) Math.sqrt(i);
    }
  }
  
  /**
   * Access a value from the square root array.
   * @param val The index of the array.
   * @return The square root of the given index (or -1 if it's too steep a
   * calculation to be useful in any of the methods).
   */
  public static int sqrt(int val)
  {
    if (val > sqrtTable.length - 1)
    {
      return -1;
    }
    else
    {
      return sqrtTable[val];
    }
  }
  
  /**
   * Returns the total euclidean distance between two points in space.
   * @param p1 A point object with which to find the distance.
   * @param p2 A point object with which to find the distance.
   * @return The euclidean distance between the two point objects.
   */
  public static int totalDistance(Point p1, Point p2)
  {
    int xComponentSqr = (p1.x - p2.x) * (p1.x - p2.x);
    int yComponentSqr = (p1.y - p2.y) * (p1.y - p2.y);
    return sqrt(xComponentSqr + yComponentSqr);
  }
  
  /**
   * Returns the manhattan distance, the total width + the total height between
   * two points.
   * @param p1 A point object with which to find the distance.
   * @param p2 A point object with which to find the distance.
   * @return The manhattan distance between the two point objects.
   */
  public static int manhattanDistance(Point p1, Point p2)
  {
    int length = Math.abs(p1.x - p2.x);
    int height = Math.abs(p1.y - p2.y);
    return length + height;
  }
  
  /**
   * Returns a potential shortest path between a given ant's location and the
   * given goal point on the map.
   * @param curAnt The ant whose position marks the start of the path.
   * @param map The map from which terrain is read to be factored into the path.
   * @param goalMap The point the ant is to reach.
   * @return A stack of directions that can be popped to enable the ant to
   * reach the given point (if the algorithm is successful).  A zero if the
   * goal parameter is invalid (on water, too far away from the ant, not a
   * valid map coordinate).  Null if the point is simply unreachable due to
   * insurmountable obstacles in the way.
   */
  public static Stack<Direction> aStar(AntData curAnt, BufferedImage map, Point goalMap)
  {
    long curTime = System.currentTimeMillis();
    if (DEBUG) System.out.println("A*: start: (" + curAnt.gridX + ", " + curAnt.gridY + ")");
    if (DEBUG) System.out.println("A*: goal: (" + goalMap.x + ", " + goalMap.y + ")");

    //First, we'll make a two-dimensional array of LocationNodes for use in A*,
    //with the ant at the center.
    
    Point startMap = new Point(curAnt.gridX, curAnt.gridY); //Map coordinates.
    
    //If the given parameters have distance greater than our search space,
    //return an empty stack and exit the method.
    
    int mapDistance = totalDistance(startMap, goalMap);
    
    if (mapDistance > ScoutData.SEARCH_RADIUS || mapDistance == -1)
    {
      return new Stack<Direction>();
    }
    
    //If our coordinate is not even on the map, we will also return an empty
    //stack.
    
    if (!(goalMap.x > 0 && goalMap.x < map.getWidth() && goalMap.y > 0 && goalMap.y < map.getHeight()))
    {
      return new Stack<Direction>();
    }
    
    //Also, if we choose a point to travel to that we know is on water, that
    //clearly isn't going to be reachable, so exit and return an empty stack in
    //that case, too.
    
    if (map.getRGB(goalMap.x, goalMap.y) == WATER_RGB)
    {
      return new Stack<Direction>();
    }
    
    //Next, construct a rectangular array with radius antSightRadius, and the
    //ant themselves at the very center of the array.
    
    LocationNode locArr[][] = new LocationNode[2 * ScoutData.SEARCH_RADIUS + 1][2 * ScoutData.SEARCH_RADIUS + 1];
    int startMapX = startMap.x - ScoutData.SEARCH_RADIUS;
    int startMapY = startMap.y - ScoutData.SEARCH_RADIUS; //Both in map coordinates,
                                                            //used to denote the top-left
                                                            //of the array.
    Point start = new Point(curAnt.gridX - startMapX, curAnt.gridY - startMapY);
    Point goal = new Point(goalMap.x - startMapX, goalMap.y - startMapY);
    //Node coordinates, to be used for array accessing.
    
    if (DEBUG) System.out.println("A*: Processing map");
    
    for (int i = 0; i < locArr.length; i++)
    {
      for (int j = 0; j < locArr[i].length; j++)
      {
        Point mapPoint = new Point(startMapX + j, startMapY + i);
        Point nodePoint = new Point(j, i); //Node coordinates, used for access
                                           //to the array.
        
        if (mapPoint.x >= 0 && mapPoint.x < map.getWidth() && mapPoint.y >= 0 && mapPoint.y < map.getHeight())
        {
          int curRGB = map.getRGB(mapPoint.x, mapPoint.y);
          if (curRGB != WATER_RGB && curRGB != NEST_RGB)
          {
            locArr[j][i] = new LocationNode(nodePoint, LandType.GRASS.getMapHeight(curRGB));
          }
          else if (curRGB == NEST_RGB)
          {
            locArr[j][i] = new LocationNode(nodePoint, -1);
            //A value of "-1" denotes the special nest height, which never
            //decreases movement speed if it is touched.
          }
          else if (curRGB == WATER_RGB)
          {
            locArr[j][i] = new LocationNode(nodePoint, -2);
            //A value of "-2" denotes an unwalkable node in the A* path.
          }
        }
        else //We're out of the bounds of the array.  Assign any such values
             //to be unwalkable.
        {
          locArr[j][i] = new LocationNode(nodePoint, -2);
        }
      }
    }
    
    //Now that we have our array of input, we begin to execute A*.  We start by
    //creating the open and closed lists that will be used throughout the
    //algorithm.
    
    PriorityQueue<LocationNode> openList = new PriorityQueue<LocationNode>();
    LinkedList<LocationNode> closedList = new LinkedList<LocationNode>();
    
    //Add the starting node to the list first.
    
    Point curP = start;
    int curG = 0;
    LocationNode curPar = null;
    LocationNode childNode;
    
    childNode = locArr[curP.x][curP.y];
    childNode.setG(curG);
    childNode.setH(manhattanDistance(goal, curP));
    childNode.setPar(null);
    openList.add(childNode);
    
    while (!closedList.contains(locArr[goal.x][goal.y]) && !openList.isEmpty())
    {
      //If we take too long, we have a 
      if (System.currentTimeMillis() - curTime >= 100)
      {
        return null;
      }

      //For each node pulled from the queue, calculate the g, h, and resulting
      //f values of the four surrounding nodes (provided they aren't solid).
      //Add the node pulled from the queue to the closed list, and add the
      //surrounding to the open list.

      curPar = openList.poll();
      curP = curPar.getP();
      curG = curPar.getG();
      int curHeight = curPar.getHeight();
      closedList.add(curPar);
      
      Point newP;
      //y-1, y+1 
      for(int i = 0; i < 8; i++)
      {
        boolean inBounds = false; //out of bounds
        int checkX = 0;
        int checkY = 0;
        //depending on direction checked, change x and y and check for index problems
        
        if (i == 0)
        {
          //North node.
          inBounds = curP.y > 0;
          checkX = curP.x;
          checkY = curP.y - 1;
        }
        else if (i == 1)
        {
          //South node.
          inBounds = curP.y < locArr[curP.x].length - 1;
          checkX = curP.x;
          checkY = curP.y + 1;
        }
        else if (i == 2)
        {
          //West node.
          inBounds = curP.x > 0;
          checkX = curP.x - 1;
          checkY = curP.y; 
        }
        else if (i == 3)
        {
          //East node.
          inBounds = curP.x < locArr.length - 1;
          checkX = curP.x+1;
          checkY = curP.y; 
        }
        else if (i == 4)
        {
          //Southeast node.
          inBounds = curP.x < locArr.length - 1 && curP.y < locArr[curP.x].length - 1;
          checkX = curP.x + 1;
          checkY = curP.y + 1; 
        }
        else if (i == 5)
        {
          //Southwest node.
          inBounds = curP.x > 0 && curP.y < locArr[curP.x].length - 1;
          checkX = curP.x - 1;
          checkY = curP.y + 1;
        }
        else if (i == 6)
        {
          //Northeast node.
          inBounds = curP.x < locArr.length - 1 && curP.y > 0;
          checkX = curP.x + 1;
          checkY = curP.y - 1;
        }
        else if (i == 7)
        {
          //Northwest node.
          inBounds = curP.x > 0 && curP.y > 0 ;
          checkX = curP.x - 1;
          checkY = curP.y - 1;
        }
        
        if (inBounds && !closedList.contains(locArr[checkX][checkY]))
        {
          newP = new Point(checkX, checkY);
          childNode = locArr[checkX][checkY];
          int weight = 1;
        
          if (childNode.getHeight() > curHeight && curHeight != -1)
          {
            weight = 2;
          }
        
          //First, address nodes directly adjacent to the parent node.
        
          if (openList.contains(childNode))
          {
            if (curG + weight < childNode.getG())
            {
            //If this node is on the list already, but it would have a lower g,
            //and thus, less total distance traveled if this were to be its
            //parent rather than the parent it currently has, we'll set its
            //parent to this parent instead, and adjust its G value
            //accordingly.
            
              childNode.setPar(curPar);
              childNode.setG(curPar.getG() + weight);
            }
          }
          else if (childNode.getHeight() != -2) //Don't want to add a solid node!
          {
            childNode.setG(curG + weight);
            childNode.setH(manhattanDistance(goal, newP));
            childNode.setPar(curPar);
            openList.add(childNode);
          }
        }
      }
    }
    
    //Now, if the path has been found, return a stack of directions to be
    //popped by the ant when moving.
    
    if (closedList.contains(locArr[goal.x][goal.y]))
    {
      Stack<Direction> dirStack = new Stack<Direction>();
      
      for (LocationNode curNode = locArr[goal.x][goal.y], parNode = null;
       	   parNode == null || !parNode.equals(locArr[start.x][start.y]);
       	   parNode = locArr[curNode.getP().x][curNode.getP().y],
       	   curNode = curNode.getPar())
      {
    	//Starting from the end node, chain back through the parents until
        //we reach the start node.
        
        if (parNode != null)
        {
          Point curNodeP = curNode.getP();
          Point parNodeP = parNode.getP();
          if (curNodeP.x < parNodeP.x && curNodeP.y < parNodeP.y)
          {
            dirStack.push(Direction.SOUTHEAST);
          }
          else if (curNodeP.x < parNodeP.x && curNodeP.y > parNodeP.y)
          {
            dirStack.push(Direction.NORTHEAST);
          }
          else if (curNodeP.x > parNodeP.x && curNodeP.y < parNodeP.y)
          {
            dirStack.push(Direction.SOUTHWEST);
          }
          else if (curNodeP.x > parNodeP.x && curNodeP.y > parNodeP.y)
          {
            dirStack.push(Direction.NORTHWEST);
          }
          else if (curNodeP.x < parNodeP.x)
          {
            dirStack.push(Direction.EAST);
          }
          else if (curNodeP.x > parNodeP.x)
          {
            dirStack.push(Direction.WEST);
          }
          else if (curNodeP.y < parNodeP.y)
          {
            dirStack.push(Direction.SOUTH);
          }
          else if (curNodeP.y > parNodeP.y)
          {
            dirStack.push(Direction.NORTH);
          }
        }
      }
      return dirStack;
    }
    else
    {
      //In this case, the goal node was not found, so there is no solution.
      //Return null to denote not to attempt AStar in this direction again, for
      //sake of computation time.

      if (DEBUG) System.out.println("AStar: NO SOLUTION");
      if (DEBUG) System.out.println("Failure time: " + (System.currentTimeMillis() - curTime));
      System.exit(1);
      return null;
    }
  }
}