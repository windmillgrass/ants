package antworld.control;
/**
 * @author Brendan Donohoe
 * @date August 22 2014
 * Utility class for AStar, used to store node information such as location,
 * g, h, weight, and parent for use in AStar.
 */

import java.awt.Point;

public class LocationNode implements Comparable<LocationNode>
{
  private Point p;
  private int g;
  private int h;
  private int height;
  private LocationNode par;
  
  /**
   * Creates a new LocationNode, a weighted point in space that can be given
   * g and h values for use in the a* algorithm.
   * @param p The node's location.
   * @param height The movement cost of entering this node on the path.  A
   * value of -1 denotes a nest node, which incurs no movement penalty
   * regardless of height value.  A value of -2 denotes a solid node that
   * cannot be touched.
   */
  public LocationNode(Point p, int height)
  {
    this.p = p;
    this.height = height;
  }
  
  /**
   * @return The node's location.
   */
  public Point getP()
  {
    return p;
  }
  
  /**
   * @param g The total distance traveled from the algorithm's starting node.
   */
  public void setG(int g)
  {
    this.g = g;
  }
  
  /**
   * @return The distance traveled from the algorithm's starting node.
   */
  public int getG()
  {
    return g;
  }
  
  /**
   * @param h The manhattan estimation of distance remaining from the current
   * node to the goal node.
   */
  public void setH(int h)
  {
    this.h = h;
  }
  
  /**
   * @return The manhattan estimation of distance remaining from the current
   * node to the goal node.
   */
  public int getH()
  {
    return h;
  }
  
  /**
   * @return The movement cost of entering this node on the path.  A value of
   * -1 denotes a solid object that cannot be moved through.
   */
  public int getHeight()
  {
    return height;
  }
  
  /**
   * @param par The node which brought this node to the open list.  Used at the
   * algorithm's end to trace the path from the start to the end node.
   */
  public void setPar(LocationNode par)
  {
    this.par = par;
  }
  
  /**
   * @return The node which brought this node to the open list.  Used at the
   * algorithm's end to trace the path from the start to the end node.
   */
  public LocationNode getPar()
  {
    return par;
  }
  
  /**
   * @return The sum of the distance traveled from the starting node and the
   * manhattan estimation of distance remaining from the current node to the
   * goal node.
   */
  public int getF()
  {
    return g + h;
  }
  
  public boolean equals(LocationNode other)
  {
    return p.equals(other.p);
  }
  
  public int compareTo(LocationNode other)
  {
    Integer thisF = getF();
    return thisF.compareTo(other.getF());
  }
  
  public String toString()
  {
    return "[LocationNode:p=" + p + "; g=" + g + "; h=" + h + "; f=" + getF() + ";]";
  }
}