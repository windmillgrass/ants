/******************************************************************************
 * ScoutData is used to choose to the ant's next action and extends AntData.
 * Contains ways for finding next direction, new directions, and choosing 
 * actions by keeping track of data fields that keep track of things such as
 * paths for the ant to follow and "states" with context-specific ant behavior.
 *****************************************************************************/

package antworld.control;

import java.awt.image.BufferedImage;
import java.awt.Point;
import java.util.Stack;

import antworld.data.AntAction.AntActionType;
import antworld.data.AntAction;
import antworld.data.AntData;
import antworld.data.AntType;
import antworld.data.CommData;
import antworld.data.Constants;
import antworld.data.Direction;
import antworld.data.FoodData;
import antworld.data.FoodType;
import antworld.data.TeamNameEnum;

public class ScoutData extends AntData
{
  /**
   * The maximum number of failed A* moves an ant can have (choosing a
   * point on water, for instance) before giving up and heading home.
   */
  public static final int MAX_STRIKES = 5;
  
  /**
   * The default depth at which the ant will search when using A*.  Will not
   * be able to calculate A* between two points whose distance surpasses this
   * value.
   */
  public static final int SEARCH_RADIUS = 80;
  
  /**
   * The default distance which the ant will search, in manhattan distance.
   * If the distance is ever greater than this value, ants will return home.
   */
  public static final int SEARCH_DIST = 600;
  
  private Direction scoutDir;
  private boolean DEBUG = false;
  private static Point nestLoc;
  private Stack<Direction> moveStack;
  private ScoutState currentState;
  private CommData gameData;
  private BufferedImage map;
  private Point foodDest;
  private int enemyPursuitID;
  
  private enum ScoutState
  {
    SEARCH, //The scout is currently looking around for food.
    GO_HOME //The scout is returning to the nest.
  };
  
  /**
   * Constructor that takes in a source AntData to copy data from using the
   * duplicating constructor in the superclass, as well as the map to use when
   * calculating A* and looking for water and the initial data information of
   * the current game.
   * @param source The AntData whose fields will be duplicated into ScoutData.
   * @param map The map with which A* and water gathering will be used.
   * @param gameData The current state of the game.
   */
  public ScoutData(AntData source, BufferedImage map, CommData gameData)
  {
	super(source);
	moveStack = new Stack<Direction>();
	currentState = ScoutState.SEARCH;
	reseedScoutDir();
	this.map = map;
	this.gameData = gameData;
	
	//If nestLoc hasn't been initialized yet, set its value.
	if (nestLoc == null)
    {
	  nestLoc = new Point();
	  nestLoc.x = gameData.nestData[gameData.myNest.ordinal()].centerX;
	  nestLoc.y = gameData.nestData[gameData.myNest.ordinal()].centerY;
    }
  }
  
  /**
   * Update all values for this scout to reflect those of the ant sent in.
   * @param source
   */
  public void update(AntData source, CommData gameData)
  {    
    gridX = source.gridX;
    gridY = source.gridY;
    alive = source.alive;

    antType = source.antType;
    carryType = source.carryType;
    carryUnits = source.carryUnits;

    myAction = new AntAction(source.myAction);

    ticksUntilNextAction = source.ticksUntilNextAction;

    health = source.health;

    underground = source.underground;
    
    this.gameData = gameData;
  }
  
  /**
   * Look around this ant and see if there's another object occupying the space
   * reached by moving in dir direction.
   * @param dir The direction to look at.
   * @return What generic object is occupying that space (null if there is
   * nothing there).
   */
  private Object hasCollision(Direction dir)
  {
    Point collisionPoint = getPointFromDir(dir);
    
    for (AntData a : gameData.myAntList)
    {
      if (a.gridX == collisionPoint.x && a.gridY == collisionPoint.y)
      {
        return a;
      }
    }
    
    for (FoodData f : gameData.foodSet)
    {
      if (f.gridX == collisionPoint.x && f.gridY == collisionPoint.y)
      {
        return f;
      }
    }
    
    for (AntData e : gameData.enemyAntSet)
    {
      if (e.gridX == collisionPoint.x && e.gridY == collisionPoint.y)
      {
        return e;
      }
    }
    return null;
  }
  
  /**
   * Return the location of the nearest food within the ant's search radius (or
   * null if there is no substantial food in sight, that is, there's less than
   * five units of food there, or it's just water).
   * @return The nearest point in which the food is visible to this ant.
   */
  private Point foodVisible()
  {
    Point minFoodLoc = null;
    int minFoodDistance = -1;
    for (FoodData f : gameData.foodSet)
    {
      Point curAntLoc = new Point(gridX, gridY);
      Point foodLoc = new Point(f.gridX, f.gridY);
      int foodDistance = Util.totalDistance(curAntLoc, foodLoc);
      if (foodDistance <= SEARCH_RADIUS && foodDistance != -1 && !f.foodType.equals(FoodType.WATER) && f.getCount() > 5)
      {
        if (foodDistance < minFoodDistance || minFoodDistance == -1)
        {
          minFoodLoc = foodLoc;
          minFoodDistance = foodDistance;
        }
      }
    }
    return minFoodLoc;
  }
  
  /**
   * Pick a new general direction for this scout to search in.  This will be
   * either a completely random direction (except for northeast or southeast,
   * which, for strategic reasons, we will not go to unless we have good reason)
   * with a 50% probability.  Otherwise, we'll choose the direction that has
   * the substantial amount of food (not water and greater than five of
   * whatever it may be).
   */
  private void reseedScoutDir()
  {
    int randVal = (int) (Math.random() * 2);
    if (randVal == 0 || gameData == null || gameData.foodSet.size() == 0)
    {
      //In this case, just pick a random direction.
      
      scoutDir = Direction.NORTHEAST;
      while (scoutDir == Direction.NORTHEAST || scoutDir == Direction.SOUTHEAST)
      {
        scoutDir = Direction.values()[2 * (int) (Math.random() * 4) + 1];
      }
    }
    else
    {
      //Otherwise, pick the direction with minimum distance between a piece of
      //substantial food and the nest.
      
      FoodData minFood = null;
      int minFoodDist = -1;
      for (FoodData f : gameData.foodSet)
      {
        if (f.getCount() > 5 && !f.foodType.equals(FoodType.WATER))
        {
          int foodDist = Util.manhattanDistance(nestLoc, new Point(f.gridX, f.gridY));
          
          if (foodDist <= minFoodDist || minFoodDist == -1)
          {
            minFood = f;
            minFoodDist = foodDist;
          }
        }
      }
      if (minFood != null)
      {
        //Might occur if there are only foods less than five in count or water
        //on the map.  In this case, just go 
        
        scoutDir = getDirFromPoint(new Point(minFood.gridX, minFood.gridY));
      }
      else
      {
        //Might occur if there are only nonsubstantial foods on the map.  In
        //this case, foodSet isn't empty, but it has nothing we want.  So just
        //go the route of picking a random direction.
        
        scoutDir = Direction.NORTHEAST;
        while (scoutDir == Direction.NORTHEAST || scoutDir == Direction.SOUTHEAST)
        {
          scoutDir = Direction.values()[2 * (int) (Math.random() * 4) + 1];
        }
      }
    }
  }
  
  /**
   * Picks a random point, but skewed in a certain direction.  
   * @param dir The direction which the ant is skewed toward.
   * @return The random point.
   */
  private Point pickRandomDirectedPoint()
  {
    int xComponent = (int) (Math.random() * SEARCH_RADIUS);
    int yComponent = (int) (Math.random() * SEARCH_RADIUS);
    
    if (scoutDir.equals(Direction.NORTH))
    {
      return new Point((gridX - SEARCH_RADIUS / 2) + xComponent, gridY - yComponent);
    }
    else if (scoutDir.equals(Direction.EAST))
    {
      return new Point(gridX + xComponent, (gridY - SEARCH_RADIUS / 2) + yComponent);
    }
    else if (scoutDir.equals(Direction.SOUTH))
    {
      return new Point((gridX - SEARCH_RADIUS / 2) + xComponent, gridY + yComponent);
    }
    else if (scoutDir.equals(Direction.WEST))
    {
      return new Point(gridX - xComponent, (gridY - SEARCH_RADIUS / 2) + yComponent);
    }
    else if (scoutDir.equals(Direction.NORTHEAST))
    {
      return new Point(gridX + xComponent, gridY - yComponent);
    }
    else if (scoutDir.equals(Direction.SOUTHEAST))
    {
      return new Point(gridX + xComponent, gridY + yComponent);
    }
    else if (scoutDir.equals(Direction.NORTHWEST))
    {
      return new Point(gridX - xComponent, gridY - yComponent);
    }
    else
    {
      return new Point(gridX - xComponent, gridY + yComponent);
    }
  }
  
  /**
   * If the nest is in view, pick a random point around the nest for the ant to
   * travel to.
   * @return The point on the nest the ant will home in on.
   */
  private Point pickRandomNestPoint()
  {
    Point pickedPoint = new Point(0, 0);
    Point curAntLoc = new Point(gridX, gridY);
    int nestDistance = Util.totalDistance(curAntLoc, pickedPoint);
    while (nestDistance > SEARCH_RADIUS || nestDistance == -1)
    {
      int xOffset = (int) (Math.random() * 21) - 10;
      int yOffset = (int) (Math.random() * 21) - 10;
      //Both offsets have some random value between -10 and 10,
      //relatively inside the nest when added to the coordinates.
      
      pickedPoint.x = nestLoc.x + xOffset;
      pickedPoint.y = nestLoc.y + yOffset;
      nestDistance = Util.totalDistance(curAntLoc, pickedPoint);
    }
    return pickedPoint;
  }
  
  /**
   * Return the given direction's opposite value.
   * @param dir A direction.
   * @return The opposite direction of dir.
   */
  private Direction backwardDirection(Direction dir)
  {
    return Direction.values()[(dir.ordinal() + 4) % 8];
  }
  
  /**
   * Return a move that will take the ant closer to water (in the event the
   * ant gets stuck, we might as well have some productivity by having them
   * nab some water).
   * @return The action that will help the ant reach/pick up water.
   */
  private AntAction moveTowardWater()
  {
    Point movePoint = getPointFromDir(scoutDir);
    
    if (map.getRGB(movePoint.x, movePoint.y) == Util.WATER_RGB)
    {
      currentState = ScoutState.GO_HOME;
      return new AntAction(AntActionType.PICKUP, scoutDir, antType.getCarryCapacity());
    }
    else
    {
      return new AntAction(AntActionType.MOVE, scoutDir);
    }
  }
  
  /**
   * Based on the direction given, return a point adjacent to that direction.
   * @param dir The direction to look in.
   * @return The point, from the current spot, relative to that direction.
   */
  private Point getPointFromDir(Direction dir)
  {
    Point movePoint;
    if (dir.equals(Direction.NORTH))
    {
      movePoint = new Point(gridX, gridY - 1);
    }
    else if (dir.equals(Direction.NORTHEAST))
    {
      movePoint = new Point(gridX + 1, gridY - 1);
    }
    else if (dir.equals(Direction.EAST))
    {
      movePoint = new Point(gridX + 1, gridY);
    }
    else if (dir.equals(Direction.SOUTHEAST))
    {
      movePoint = new Point(gridX + 1, gridY + 1);
    }
    else if (dir.equals(Direction.SOUTH))
    {
      movePoint = new Point(gridX, gridY + 1);
    }
    else if (dir.equals(Direction.SOUTHWEST))
    {
      movePoint = new Point(gridX - 1, gridY + 1);
    }
    else if (dir.equals(Direction.WEST))
    {
      movePoint = new Point(gridX - 1, gridY);
    }
    else
    {
      movePoint = new Point(gridX - 1, gridY - 1);
    }
    return movePoint;
  }
  
  /**
   * Given a point, determine what direction from the current location that
   * spot lies in.
   * @param p The destination from the current location to be evaluated.
   * @return A direction reflecting the general direction that can be followed
   * from the current point to reach the destination point.
   */
  private Direction getDirFromPoint(Point p)
  {
    double width = p.x - gridX;
    double height = p.y - gridY;
    boolean isEast = width > 0;
    boolean isSouth = height > 0;
    
    //Get a fractional, less than one value for the ratio.
    double ratio;
    if (width > height)
    {
      ratio = Math.abs(height / width);
    }
    else
    {
      ratio = Math.abs(width / height);
    }
    
    if (ratio < .5)
    {
      //In this case, one of the directions is far greater than the other.
      //Return a strict horizontal/vertical direction.
      
      if (isEast && Math.abs(width) > Math.abs(height))
      {
        return Direction.EAST;
      }
      else if (!isEast && Math.abs(width) > Math.abs(height))
      {
        return Direction.WEST;
      }
      else if (isSouth && Math.abs(width) < Math.abs(height))
      {
        return Direction.SOUTH;
      }
      else
      {
        return Direction.NORTH;
      }
    }
    else
    {
      //The directions didn't differ much in magnitude from each other.  So,
      //return a diagonal direction.
      
      if (isEast && isSouth)
      {
        return Direction.SOUTHEAST;
      }
      else if (isEast && !isSouth)
      {
        return Direction.NORTHEAST;
      }
      else if (!isEast && isSouth)
      {
        return Direction.SOUTHWEST;
      }
      else
      {
        return Direction.NORTHWEST;
      }
    }
  }
  
  /**
   * Look around.  If there's a brainless bot one tile away, attack it.
   * @return A direction denoting the brainless bot's direction from the ant's
   * current location, or null if there is no such bot.
   */
  private Direction getEnemyPos()
  {
    for (AntData e : gameData.enemyAntSet)
    {
      if (e.teamName.equals(TeamNameEnum.NEARLY_BRAINLESS_BOTS))
      {
        int antDist = Util.totalDistance(new Point(gridX, gridY), new Point(e.gridX, e.gridY));
        if (antDist == 1)
        {
          Direction collisionDir = scoutDir;
          for (int i = 0; i < 8; i++)
          {
            Point enemyPoint = getPointFromDir(collisionDir);
            if (e.gridX == enemyPoint.x && e.gridY == enemyPoint.y)
            {
              return collisionDir;
            }
            collisionDir = Direction.getRightDir(collisionDir);
          }
        }
      }
    }
    return null;
  }
  
  /**
   * Only called if hostile ants (brainless bots) are near the nest.  If so,
   * enemy ants within SEARCH_RADIUS of the nest are targeted by ants also
   * within SEARCH_RADIUS of the ant to chase after them and kill them by
   * supplying the enemy coordinates of a random ant near the nest (if no such
   * ant has been supplied already) or the updated coordinate of that ant
   * retrieved earlier (by saving the enemy ID and looking for the ant's
   * position).
   * @return A point denoting the location of the ant currently being pursued.
   */
  private Point checkForEnemy()
  {
    if (enemyPursuitID != 0)
    {
      for (AntData e : gameData.enemyAntSet)
      {
        if (enemyPursuitID == e.id)
        {
          //We are pursuing an enemy near our nest, update the point with which
          //to follow the enemy.
          
          int enemyDist = Util.totalDistance(new Point(gridX, gridY), new Point(e.gridX, e.gridY));
          int nestDist = Util.totalDistance(new Point(e.gridX, e.gridY), nestLoc);
          if (enemyDist <= SEARCH_RADIUS && enemyDist != -1 &&
              nestDist <= SEARCH_RADIUS && nestDist != -1)
          {
            //If the ant is within both this ant's radius and the nest radius,
            //we'll continue pursuit.
            
            if (DEBUG) System.out.println("SCOUTDATA: Pursuing BB " + e + " at " + new Point(e.gridX, e.gridY));
            return new Point(e.gridX, e.gridY);
          }
        }
      }
      enemyPursuitID = 0;
    }
    
    for (AntData e : gameData.enemyAntSet)
    {
      //We're currently pursuing no enemies near our nest.  Look to see if
      //there's a brainless bot near both us and the nest.  If there is, get
      //its coordinate and its id to follow it as it moves.
      
      if (e.teamName.equals(TeamNameEnum.NEARLY_BRAINLESS_BOTS))
      {
        Point enemyPoint = new Point(e.gridX, e.gridY);
        int enemyDist = Util.totalDistance(new Point(gridX, gridY), enemyPoint);
        int nestDist = Util.totalDistance(new Point(enemyPoint), nestLoc);
        if (enemyDist <= SEARCH_RADIUS && enemyDist != -1 && nestDist <= SEARCH_RADIUS && nestDist != -1)
        {
          if ((antType.equals(AntType.ATTACK) || antType.equals(AntType.DEFENCE))
        	  && enemyDist <= SEARCH_RADIUS && enemyDist != -1 && nestDist <= SEARCH_RADIUS && nestDist != -1)
          {
            //If we're an attack or defense ant, we will be a bit more
            //tenacious about pursuing our enemies.
            
            enemyPursuitID = e.id;
            if (DEBUG) System.out.println("Pursuing BB " + e + " at " + enemyPoint);
            return enemyPoint;
          }
          if (enemyDist <= SEARCH_RADIUS / 4)
          {
            //Otherwise, they'll need to be a fair bit closer for us to chase
            //after them.
            
            enemyPursuitID = e.id;
            if (DEBUG) System.out.println("SCOUTDATA: Pursuing BB " + e + " at " + enemyPoint);
            return enemyPoint;
          }
        }
      }
    }
    return null;
  }
  
  /**
   * Called each turn - based on the number of ants in sight radius of the
   * current ant, determine if the ant should continue in their search, or
   * return home if the forces in view look overwhelming.
   * @return A boolean that, if true, denotes that the ant should return home.
   */
  private boolean isCoward()
  {
    int bravery;
    if (antType.equals(AntType.VISION)) //Larger viewing radius means more ants
                                        //may be in sight at a given time, so
                                        //more let may be needed.
    {
      bravery = 3;
    }
    if (antType.equals(AntType.DEFENCE) || antType.equals(AntType.ATTACK))
    {
      bravery = 2;
    }
    else
    {
      bravery = 1;
    }
    
    int cowardice = 0;
    Point antLoc = new Point(gridX, gridY);
    int nestDist = Util.totalDistance(antLoc, nestLoc);
    for (AntData e : gameData.enemyAntSet)
    {
      int enemyDist = Util.totalDistance(antLoc, new Point(e.gridX, e.gridY));
      if (enemyDist <= antType.getVisionRadius() && enemyDist != -1 && (nestDist >= SEARCH_RADIUS || nestDist == -1))
      {
        //If the enemy ant is in view and is not near the nest (we'll assume
        //the ants get a rush of courage if ants are near the nest), add one to
        //the ant's likeliness to flee.
        
        cowardice++;
      }
    }
    
    if (cowardice > bravery)
    {
      return true;
    }
    else
    {
      return false;
    }
  }
  
  /**
   * Utility function that prints various information about the given ant.
   */
  private void printDebugInfo()
  {
    System.out.println("SCOUTDATA: Original AntData: " + super.toString());
    System.out.println("SCOUTDATA: moveStack: " + moveStack);
    System.out.println("SCOUTDATA: currentState: " + currentState);
    System.out.println("scoutDir: " + scoutDir);
  }

  /**
   * Preliminary actions for an ant to make regardless of state.  Mostly
   * involves checking information regarding enemies, and what to do regarding
   * those enemies.
   * @return An attack action if an enemy is nearby.  Otherwise, null (though
   * might update the moveStack
   */
  private AntAction preliminaryActions()
  {
    AntAction returnAction = null;

    //If there's an enemy one square away, attack it if victory (or at least,
    //a safe escape) appears likely.
    Direction attackDir = getEnemyPos();
    if (attackDir != null && (health > antType.getMaxHealth() / 2 || antType.equals(AntType.DEFENCE) 
        || antType.equals(AntType.ATTACK)))
    {
      returnAction  = new AntAction(AntActionType.ATTACK, attackDir);
    }
    
    //If an enemy is near the nest, send this ant after them (if they aren't 
    //carrying anything and one of: they are at greater than 3/4 HP, are an
    //attack ant, or a defense ant).  Don't return an action, but update the
    //ant's movement path if such an action is appropriate.
    if (carryUnits == 0 && (health > 3 * antType.getMaxHealth() / 4 ||  antType.equals(AntType.ATTACK)
        || antType.equals(AntType.DEFENCE)))
    {
      Point attackPoint = checkForEnemy();
      if (attackPoint != null)
      {
        moveStack = Util.aStar(this, map, attackPoint);
      }
    }
    return returnAction;
  }
  
  /**
   * Actions an ant will take if it is currently searching the map for food.
   * Generally, these ants search each turn for substantial food, unless
   * enemies intervene to the point where it's in the ant's best interest
   * to simply return to the nest.
   * @return The next action an ant in the search state will take.
   */
  private AntAction searchActions()
  {
    AntAction returnAction = null;
    
    //First, if the ant has less than full HP, and is in the radius of the
    //nest, send it in to heal.
    if (myAction.type.equals(AntActionType.DROP) && health <= antType.getMaxHealth())
    {
      if (DEBUG) System.out.println("SCOUTDATA: SEARCH: Ant entering the nest.");
      returnAction = new AntAction(AntActionType.ENTER_NEST);
      return returnAction;
    }
    
    //In the event of a reset, if the ant is carrying something or it's too
    //far away (SEARCH_DIST) from the nest center, send it home.
    if (carryUnits > 0 || Util.manhattanDistance(new Point(gridX, gridY), nestLoc) > SEARCH_DIST)
    {
      currentState = ScoutState.GO_HOME;
      returnAction = new AntAction(AntActionType.STASIS);
      return returnAction;
    }
    
    //If the path ahead is looking treacherous, head back to the nest rather
    //than heading into the dragon's den.  That is, if there are an
    //overwhelming number of ants in view, health is less than a third, or
    //we're too far away from the home nest, return home.
    if (isCoward() || health < antType.getMaxHealth() / 2 || 
        Util.manhattanDistance(new Point(gridX, gridY), nestLoc) > SEARCH_DIST)
    {
      moveStack.removeAll(moveStack);
      currentState = ScoutState.GO_HOME;
      returnAction = new AntAction(AntActionType.STASIS);
      return returnAction;
    }
    
    //If food is visible at ANY point during the ant's search, have it stop
    //what it's doing (provided it's a substantial amount of food and not
    //relatively useless water) and grab the food.
    if (foodDest == null && enemyPursuitID == 0)
    {
      Point foodLoc = foodVisible();
      if (foodLoc != null)
      {
        if (DEBUG) System.out.println("SCOUTDATA: SEARCH: New food found.");
        foodDest = new Point(foodLoc); //Track the point of the food we found.
        moveStack = Util.aStar(this, map, foodDest);
      }
    }
    
    //If the current path the ant has followed has ended, get a new one.
    if (moveStack == null || moveStack.size() == 0)
    {
      if (DEBUG) System.out.println("SEARCH: Getting more moves.");
      
      //If the moveStack is empty, foodDest will be null, as if foodDest was
      //previously not null, yet we're still in search, the food must have
      //left the spot before we got to it.  Continue on the search regardless.
      
      foodDest = null;
      
      //As no food is in view yet, simply pick another point in the ant's
      //general search direction to travel to.

      moveStack = Util.aStar(this, map, pickRandomDirectedPoint());
      
      if (DEBUG) System.out.println("SCOUTDATA: SEARCH: Moves obtained: " + moveStack);
      
      //In the event an ant has run out of space to search (aStar cannot reach
      //MAX_STRIKES many random points without fail, thus returning either 0
      //or null), make more attempts to choose a valid point, ending if ten
      //erroneous coordinates are picked (0 is returned) or a single more
      //costly "NO SOLUTION" choice is made (null is returned).

      int strikes = 0;
      while (moveStack == null || moveStack.size() == 0)
      {
        if (moveStack == null)
        {
          currentState = ScoutState.GO_HOME;
          moveStack = new Stack<Direction>();
          returnAction = new AntAction(AntActionType.STASIS);
          return returnAction;
        }
        strikes++;
        if (strikes >= MAX_STRIKES)
        {
          //Often, an ant is by water when this occurs.  So, have it pick
          //some up to carry to the nest using the moveTowardWater()
          //function.
      	  
          if (DEBUG) System.out.println("SCOUTDATA: SEARCH: Ant has become stuck, moving toward water.");
          returnAction = moveTowardWater();
          return returnAction;
        }
        if (DEBUG) System.out.println("SCOUTDATA: Strike " + strikes + ": Retrying A*.");
        moveStack = Util.aStar(this, map, pickRandomDirectedPoint());
      }
    }
    
    //Look at the next space and see if there is anything there that might
    //cause collision.  If there is, act accordingly.
    
    Object collisionObj = hasCollision(moveStack.peek());
    
    if (collisionObj != null)
    {
      if (DEBUG) System.out.print("SCOUTDATA: SEARCH: Ant has collision with");
      if (collisionObj instanceof AntData)
      {
        AntData antCollision = (AntData) collisionObj;
        if (gameData.myAntList.contains(antCollision))
        {
          if (DEBUG) System.out.print(" ally ant " + antCollision);
      	  //If there's an ally in the way...
          
          if (!myAction.type.equals(AntActionType.STASIS))
          {
            if (DEBUG) System.out.println(" and is waiting in stasis.");
            
            //If we moved previously, try just waiting to see if it'll be out
            //of our way next turn.
            
            returnAction =  new AntAction(AntActionType.STASIS);
            return returnAction;
          }
          else
          {
            //All right, it isn't moving out of the way and both of the ants
            //are stuck.  Break the current ant's path, pick a random
            //(walkable) direction, and go around it.  A new path will be
            //recalculated next turn.
            
            moveStack.removeAll(moveStack);
            Direction dirCandidate = Direction.NORTH;
            for (int i = 0; i < 8; i++)
            {
              dirCandidate = Direction.getRightDir(dirCandidate);
              if (hasCollision(dirCandidate) == null)
              {
                if (DEBUG) System.out.println("and is moving in " + dirCandidate);
                returnAction = new AntAction(AntActionType.MOVE, dirCandidate);
                return returnAction;
              }
            }
            if (DEBUG) System.out.println("and is surrounded and waiting in stasis.");
            returnAction = new AntAction(AntActionType.STASIS);
            return returnAction;
          }
        }
        else
        {
          if (DEBUG) System.out.println(" enemy ant " + antCollision);
          
          //An enemy is in our way.  Attack it.
          
          returnAction = new AntAction(AntActionType.ATTACK, moveStack.peek());
          return returnAction;
        }
      }
      else if (collisionObj instanceof FoodData)
      {
        //Pick up the food we've collided with and head back to the nest to
        //drop it off.
        
        FoodData pickedUpFood = (FoodData) collisionObj;
        if (DEBUG) System.out.println(" food " + pickedUpFood);
        Direction finalDir = moveStack.pop();
        moveStack.removeAll(moveStack);
        currentState = ScoutState.GO_HOME;
        foodDest = null;
        returnAction = new AntAction(AntActionType.PICKUP, finalDir, Math.min(pickedUpFood.getCount(), 
                                     antType.getCarryCapacity()));
        return returnAction;
      }
    }
    returnAction =  new AntAction(AntActionType.MOVE, moveStack.pop());
    return returnAction;
  }
  
  /**
   * Actions the ant will make if their priority is to make it back to the
   * nest to drop food, heal, and/or get out of danger.  Generally, an ant
   * in this state is concerned only with making it back to the nest.
   * @return The next action an ant in the go-home state will take.
   */
  private AntAction homeActions()
  {
    AntAction returnAction = null;
    if (Util.manhattanDistance(new Point(gridX, gridY), nestLoc) < Constants.NEST_RADIUS - 2)
    {
      //We're at the nest.  Drop the food in a random location (doesn't
      //really matter, as it'll be absorbed by the nest) and return to
      //searching.
      
      currentState = ScoutState.SEARCH;
      moveStack.removeAll(moveStack);
      if (carryUnits > 0)
      {
        returnAction = new AntAction(AntActionType.DROP, Direction.getRandomDir(), carryUnits);
        return returnAction;
      }
      else
      {
        returnAction = new AntAction(AntActionType.ENTER_NEST);
        return returnAction;
      }
    }
    
    //If the current path the ant has followed has ended, get a new one.
    if (moveStack == null || moveStack.size() == 0)
    {
      if (DEBUG) System.out.println("SCOUTDATA: GO_HOME: Getting more moves.");
      
      int distanceToNest = Util.totalDistance(new Point(gridX, gridY), nestLoc);
      if (distanceToNest >= SEARCH_RADIUS || distanceToNest == -1)
      {
        if (DEBUG) System.out.println("SCOUTDATA: GO_HOME: Nest location not in view, picking random point on nest.");

        //If the nest isn't in sight, pick a point skewed in the general
        //direction of the nest to travel to.
        
        scoutDir = getDirFromPoint(nestLoc);
        moveStack = Util.aStar(this, map, pickRandomDirectedPoint());
        
        int strikes = 0;
        while (moveStack == null || moveStack.size() == 0)
        {
          strikes++;
          if (strikes >= MAX_STRIKES || moveStack == null)
          {
            if (DEBUG) System.out.println("SCOUTDATA: GO_HOME: Ant has become stuck.  Picking another random direction.");
            
            //If we're stuck when moving back to the nest, just pick a random
            //point to travel to in sight in an attempt to get unstuck.
            
            reseedScoutDir();
          }
          if (DEBUG) System.out.println("SCOUTDATA: GO_HOME: Strike " + strikes + ": Retrying A*.");
          moveStack = Util.aStar(this, map, pickRandomDirectedPoint());
        }
      }
      else
      {
        if (DEBUG) System.out.println("SCOUTDATA: GO_HOME: Nest in view.");
        
        //The nest is in view - pick a random point on the nest to travel to.

        if (DEBUG) System.out.println("GO_HOME: Not in nest vicinity, picking random nest point.");
        moveStack = Util.aStar(this, map, pickRandomNestPoint());
        
        int strikes = 0;
        while (moveStack.size() == 0)
        {
          strikes++;
          if (strikes >= MAX_STRIKES)
          {
            //And if we get stuck in the process, again, pick a random point to
            //travel to.
            
            reseedScoutDir();
            moveStack = Util.aStar(this, map, pickRandomDirectedPoint());
          }
          moveStack = Util.aStar(this, map, pickRandomNestPoint());
        }
      }
    }
    
    Object collisionObj = hasCollision(moveStack.peek());
    if (collisionObj != null)
    {
      if (DEBUG) System.out.print("SCOUTDATA: GO_HOME: Ant has collision with");
      if (collisionObj instanceof AntData)
      {
        AntData antCollision = (AntData) collisionObj;
        if (gameData.myAntList.contains(antCollision))
        {
          //We've collided with an ally.  First, just try waiting to see if
          //they'll get out of our way.
          
          if (DEBUG) System.out.print(" ally ant " + antCollision);
      	  if (!myAction.type.equals(AntActionType.STASIS))
          {
      	    if (DEBUG) System.out.println(" and is waiting in stasis.");
            returnAction = new AntAction(AntActionType.STASIS);
            return returnAction;
          }
          else
          {
            //They will not, so let's break off our path and pick a random
            //direction in an effort to get around them.
            
            moveStack.removeAll(moveStack);
            Direction dirCandidate = Direction.NORTH;
            for (int i = 0; i < 8; i++)
            {
              if (hasCollision(dirCandidate) == null)
              {
                if (DEBUG) System.out.println(" and is moving in " + dirCandidate);
                return new AntAction(AntActionType.MOVE, dirCandidate);
              }
              dirCandidate = Direction.getRightDir(dirCandidate);
            }
            if (DEBUG) System.out.println(" and is surrounded and waiting in stasis.");
            returnAction = new AntAction(AntActionType.STASIS);
            return returnAction;
          }
        }
        else
        {
          //An enemy is in our way.  Attack it.
          
          if (DEBUG) System.out.println(" enemy ant " + antCollision);
          returnAction = new AntAction(AntActionType.ATTACK, moveStack.peek());
          return returnAction;
        }
      }
      else if (collisionObj instanceof FoodData)
      {
        if (carryUnits != antType.getCarryCapacity())
        {
          //Pick up the food we've collided with and head back to the nest to
          //drop it off if we can carry it.
          
          FoodData pickedUpFood = (FoodData) collisionObj;
          if (DEBUG) System.out.println(" food " + pickedUpFood);
          returnAction = new AntAction(AntActionType.PICKUP, moveStack.peek(), Math.min(pickedUpFood.getCount(), 
                                       antType.getCarryCapacity() - carryUnits));
          return returnAction;
        }
        else
        {
          //Otherwise, simply move around it.
          
          moveStack.removeAll(moveStack);
          Direction dirCandidate = Direction.NORTH;
          for (int i = 0; i < 8; i++)
          {
            if (hasCollision(dirCandidate) == null)
            {
              if (DEBUG) System.out.println(" food, but can't pick any up and is moving in " + dirCandidate);
              returnAction = new AntAction(AntActionType.MOVE, dirCandidate);
              return returnAction;
            }
            dirCandidate = Direction.getRightDir(dirCandidate);
          }
          if (DEBUG) System.out.println(" food, but is surrounded and waiting in stasis.");
          returnAction = new AntAction(AntActionType.STASIS);
          return returnAction;
        }
      }
    }
    
    returnAction = new AntAction(AntActionType.MOVE, moveStack.pop());
    return returnAction;
  }
  
  /**
   * Based on the ant's current state, return its next move for the turn.
   * @return The next AntAction this ant will take.
   */
  public AntAction nextMove()
  { 
    if (DEBUG) printDebugInfo();
    
    AntAction prelimAct = preliminaryActions();
    if (prelimAct != null)
    {
      return prelimAct;
    }
    
    if (currentState.equals(ScoutState.SEARCH))
    {
      return searchActions();
    }
    
    else if (currentState == ScoutState.GO_HOME)
    {
      return homeActions();
    }
    
    return new AntAction(AntActionType.STASIS);
  }
}